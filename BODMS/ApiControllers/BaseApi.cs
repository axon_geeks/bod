﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using Humanizer;
using BODMS.DataAccess.Filters;
using BODMS.DataAccess.Repositories;
using BODMS.AxHelpers;
using BODMS.Models;

namespace BODMS.ApiControllers
{
    [Authorize]
    public class BaseApi<T> : ApiController where T : class
    {
        protected BaseRepository<T> Repository = new BaseRepository<T>();
        private readonly string _klassName = typeof(T).Name.Humanize(LetterCasing.Title);

        public virtual ResultObj Get(long id)
        {
            ResultObj result;
            try
            {
                var data = Repository.Get(id);
                result = WebHelpers.BuildResponse(data, "", true, 1);
            }
            catch(Exception ex)
            {
                result = WebHelpers.ProcessException(ex);
            }
            return result;
        }

        public virtual ResultObj Get()
        {
            ResultObj result;
            try
            {
                var data = Repository.Get();
                result = WebHelpers.BuildResponse(data, "Records Loaded", true, data.Count());
            }
            catch (Exception ex)
            {
                result = WebHelpers.ProcessException(ex);
            }
            return result;
        }


        public virtual ResultObj Post(T record)
        {
            ResultObj result;
            try
            {
                Repository.Insert(SetAudit(record, true));
                result = WebHelpers.BuildResponse(record, $"New {_klassName} saved successfully",true,1);
            } catch(Exception ex)
            {
                result = WebHelpers.ProcessException(ex);
            }

            return result;
        }

        public virtual ResultObj Put(T record)
        {
            ResultObj result;
            try
            {
                Repository.Insert(SetAudit(record));
                result = WebHelpers.BuildResponse(record, $"New {_klassName} updated successfully", true, 1);
            }
            catch (Exception ex)
            {
                result = WebHelpers.ProcessException(ex);
            }

            return result;
        }

        public virtual ResultObj Delete(long id)
        {
            ResultObj result;
            try
            {
                Repository.Delete(id);
                result = WebHelpers.BuildResponse(id, $"New {_klassName} deleted successfully", true, 1);
            }
            catch (Exception ex)
            {
                result = WebHelpers.ProcessException(ex);
            }

            return result;
        }

        protected T SetAudit(T record, bool isNew = false)
        {
            if (isNew)
            {
                if (typeof(T).GetProperty(GenericProperties.CreatedBy) != null)
                    typeof(T).GetProperty(GenericProperties.CreatedBy).SetValue(record, User.Identity.Name);
            }

            if (typeof(T).GetProperty(GenericProperties.ModifiedBy) != null)
                typeof(T).GetProperty(GenericProperties.ModifiedBy).SetValue(record, User.Identity.Name);

            return record;
        }

       // Todo: due 2016-05-16 Implement Soft Delete
        public virtual ResultObj SoftDelete(long id)
        {
            ResultObj result = new ResultObj();
            return result;
        }

    }
}