using System.Collections.Generic;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity.Migrations;
using BODMS.Models;

namespace BODMS.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using Microsoft.AspNet.Identity.EntityFramework;
    using Microsoft.AspNet.Identity;
    internal sealed class Configuration : DbMigrationsConfiguration<BODMS.Models.AppDbContext>
    {
        public Configuration() : this(new UserManager<User>(new UserStore<User>(new BODMS.Models.AppDbContext())))
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
        }

        public UserManager<User> UserManager { get; private set; }

        public Configuration(UserManager<User> userManager) { UserManager = userManager; }

        protected override void Seed(BODMS.Models.AppDbContext db)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    db.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //

            var roles = new List<IdentityRole>
            {
                new IdentityRole {Name = Privileges.CanActivateEmployees},
                new IdentityRole {Name = Privileges.CanApproveAmendments},
                new IdentityRole {Name = Privileges.CanDisapproveAmendments},
                new IdentityRole {Name = Privileges.CanEditSettings},
                new IdentityRole {Name = Privileges.CanRequestAmendments},
                new IdentityRole {Name = Privileges.CanViewAmendments},
                new IdentityRole {Name = Privileges.CanViewDashboard},
                new IdentityRole {Name = Privileges.CanViewEmployees},
                new IdentityRole {Name = Privileges.CanViewSettings},
                new IdentityRole {Name = Privileges.CanViewEmployeeData},
                new IdentityRole {Name = Privileges.CanViewPersonalInfo},
                new IdentityRole {Name = Privileges.CanManageUsers}
            };

            roles.ForEach(r => db.Roles.AddOrUpdate(q => q.Name, r));
            var a = "";
            roles.ForEach(q => a += q.Name + ",");

            var adminProfile = new Profile
            {
                Id = 1,
                Name = ConfigKeys.Administrator,
                Notes = "Administrator Role",
                Privileges = a.Trim(','),
                Locked = false
            };
            db.Profiles.AddOrUpdate(adminProfile);

            var milAlpha = new List<MilitaryAlphabet>
            {
                new MilitaryAlphabet {Alphabet = "A", Code = "Alpha"},
                new MilitaryAlphabet {Alphabet = "B", Code = "Bravo"},
                new MilitaryAlphabet {Alphabet = "C", Code = "Charlie"},
                new MilitaryAlphabet {Alphabet = "D", Code = "Delta"},
                new MilitaryAlphabet {Alphabet = "E", Code = "Echo"},
                new MilitaryAlphabet {Alphabet = "F", Code = "Foxtrot"},
                new MilitaryAlphabet {Alphabet = "G", Code = "Golf"},
                new MilitaryAlphabet {Alphabet = "H", Code = "Hotel"},
                new MilitaryAlphabet {Alphabet = "I", Code = "India"},
                new MilitaryAlphabet {Alphabet = "J", Code = "Juliet"},
                new MilitaryAlphabet {Alphabet = "K", Code = "Kilo"},
                new MilitaryAlphabet {Alphabet = "L", Code = "Lima"},
                new MilitaryAlphabet {Alphabet = "M", Code = "Mike"},
                new MilitaryAlphabet {Alphabet = "N", Code = "November"},
                new MilitaryAlphabet {Alphabet = "O", Code = "Oscar"},
                new MilitaryAlphabet {Alphabet = "P", Code = "Papa"},
                new MilitaryAlphabet {Alphabet = "Q", Code = "Quebec"},
                new MilitaryAlphabet {Alphabet = "R", Code = "Romeo"},
                new MilitaryAlphabet {Alphabet = "S", Code = "Sierra"},
                new MilitaryAlphabet {Alphabet = "T", Code = "Tango"},
                new MilitaryAlphabet {Alphabet = "U", Code = "Uniform"},
                new MilitaryAlphabet {Alphabet = "V", Code = "Victor"},
                new MilitaryAlphabet {Alphabet = "W", Code = "Whiskey"},
                new MilitaryAlphabet {Alphabet = "X", Code = "X-ray"},
                new MilitaryAlphabet {Alphabet = "Y", Code = "Yankee"},
                new MilitaryAlphabet {Alphabet = "Z", Code = "Zulu"}
            };
            milAlpha.ForEach(m => db.MilitaryAlphabets.AddOrUpdate(x=>x.Alphabet,m));


            var userManager = new UserManager<User>(new UserStore<User>(db))
            {
                UserValidator = new UserValidator<User>(UserManager)
                {
                    AllowOnlyAlphanumericUserNames = false
                }
            };

            //Admin User
            if (UserManager.FindByNameAsync("admin").Result != null) return;
            var res = userManager.CreateAsync(new User
            {
                Name = ConfigKeys.Administrator,
                ProfileId = adminProfile.Id,
                UserName = "admin",
                CreatedAt = DateTime.Now,
                UpdatedAt = DateTime.Now,
                Locked = true,
            }, "password");

            if (!res.Result.Succeeded) return;
            var userId = userManager.FindByNameAsync("admin").Result.Id;
            roles.ForEach(q => userManager.AddToRole(userId, q.Name));

            var ranks = new List<Rank>
            {
                new Rank {Name="Leutenant"},
                new Rank {Name="Captain"},
                new Rank {Name="Major"},
                new Rank {Name="Leutenant Colonel"},
                new Rank {Name="Colonel"},
            };
            ranks.ForEach(r => db.Ranks.AddOrUpdate(x => x.Name, r));

            db.AuthSources.AddOrUpdate(x => x.Name, new AuthSource {Name = "GHQ(ORD)"});
            db.AuthUnits.AddOrUpdate(x => x.Name, new AuthUnit {Name = "GHQ(COS)"});
            db.AuthTypes.AddOrUpdate(x => x.Name, new AuthType { Name = "RESTRICTED"});

            db.SaveChanges();
        }
    }
}
