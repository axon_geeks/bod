﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using BODMS.Models;

namespace BODMS.Models
{
    public class Item : Auditable
    {
        [Index (IsUnique = true), Required]
        public int PartNumber { get; set; }

        [Required]
        public string Name { get; set; }
        public string Description { get; set; }

        [Required]
        public long VaoSectionId { get; set; }
        public virtual VaosSection VaosSection { get; set; }

        [Required]
        public long ItemUnitId { get; set; }
        public virtual ItemUnit ItemUnit { get; set; }
    }

    public class VaosSection : LookUp
    {
        public long StoreId { get; set; }
        public virtual Store Store { get; set; }
    }

    public class Store : LookUp
    {
        public virtual ICollection<VaosSection> VaosSections { get; set; }

        public string OfficerInChargeId { get; set; }
        public virtual User OfficerInCharge { get; set; }

        public string SecondInCommandId { get; set; }
        public virtual User SecondInCommand { get; set; }

        public virtual ICollection<Item> Items { get; set; }
    }

    public class D982 : Auditable
    {
        public string DemandNumber { get; set; }

        [Required]
        public DateTime DemandDate { get; set; }

        [Required, Index(IsUnique = true)]
        public int ControlNumber { get; set; }

        [Required]
        public DateTime ReceivedDate { get; set; }
        public string SpecialInstruction { get; set; }

        public long AuthorityId { get; set; }
        public virtual Authority Authority { get; set; }

        [Required]
        public string DemandingOfficer { get; set; }

        public long D982ApprovalId { get; set; }
        public virtual D982Approval D982Approval { get; set; }
    }

    public enum D982ApprovalTypes { IndentChecked, Posted, PostingChecked, ApprovingOfficer, TrafficChecker, ReceiptChecker }

    public class D982Approval : Auditable
    {
        [ForeignKey("D982")]
        public long D982Id { get; set; }
        public virtual D982 D982 { get; set; }

        [Required]
        public DateTime ApprovalDate { get; set; }

        [Required]
        public virtual D982ApprovalTypes Type { get; set; }

        [Required]
        public string UserId { get; set; }
        public virtual User User { get; set; }
    }

    public class DailyReceiptSheet : Auditable
    {
        [Required]
        public DateTime ReceiptDateTime { get; set; }

        [Required, Index(IsUnique = true)]
        public int Number { get; set; }

        [Required]
        public string Consignor { get; set; }

        [Required]
        public long AuthorityId { get; set; }
        public virtual Authority Authority { get; set; } 
        
        [Required]
        public long DailyReceiptSheetDetailId { get; set; }
        public virtual DailyReceiptSheetDetail DailyReceiptSheetDetail { get; set; }
    }

    public class DailyReceiptSheetDetail : HasId
    {
        [Required]
        public long DailyReceiptSheetId { get; set; }
        public virtual DailyReceiptSheet DailyReceiptSheet { get; set; }

        [Required]
        public virtual Item Item { get; set; }
        public long ItemId { get; set; }

        public bool Sealed { get; set; }

        [Required]
        public int Quantity { get; set; }
    }

    public class ReceiptVoucher : Auditable
    {
        public virtual DailyReceiptSheet DailyReceiptSheet { get; set; }
        public long DailyReceiptSheetId { get; set; }
    }

    public class ReceiptVoucherDetail : HasId
    {
        public long ReceiptVoucherId { get; set; }

        public virtual Item Item { get; set; }
        public long ItemId { get; set; }

        public int Quantity { get; set; }
    }

    public class Loan : Auditable
    {
        public DateTime LoanStartDate { get; set; }
        public DateTime EndStartDate { get; set; }

        public virtual Authority Authority { get; set; }
        public long AuthorityId { get; set; }

        public virtual Unit Unit { get; set; }
        public long UnitId { get; set; }

        public string Name { get; set; }

        public virtual Rank Rank { get; set; }
        public long RankId { get; set; }

        public string Remarks { get; set; }
        public string Signature { get; set; }

        public virtual ICollection<LoadDetail> LoanDetails { get; set; }
    }

    public class LoadDetail : HasId
    {
        public virtual Item Item { get; set; }
        public long  ItemId { get; set; }
        public int Quantity { get; set; }
    }
}