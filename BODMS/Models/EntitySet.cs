﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;
using System.Security.Cryptography.X509Certificates;

namespace BODMS.Models
{
   public interface IHasId
    {
        [Key]
        long Id { get; set; }
    }

    public interface ISecured
    {
        bool Locked { get; set; }
        bool Hidden { get; set; }
    }

    public interface IAuditable: IHasId
    {
        [Required]
        string CreatedBy { get; set; }
        [Required]
        string ModifiedBy { get; set; }
        DateTime CreatedAt { get; set; }
        DateTime ModifiedAt { get; set; }
    }

    public class HasId : IHasId
    {
        public long Id { get; set; }
    }

    public class Auditable : HasId, IAuditable
    {
        [Required]
        public string CreatedBy { get; set; }
        [Required]
        public string ModifiedBy { get; set; }
        public  DateTime CreatedAt { get; set; }
        public DateTime ModifiedAt { get; set; }
    }

    public class AuditFields : Auditable, ISecured
    {
       public bool Locked { get; set; }
       public bool Hidden { get; set; }
    }

    public class LookUp : AuditFields
    {
        [MaxLength(512), Required, Index(IsUnique = true)]
        public string Name { get; set; }
        [MaxLength(1000)]
        public string Notes { get; set; }
    }

    public class Profile : HasId
    {
        [Required, MaxLength(512)]
        public string Name { get; set; }
        [MaxLength(1000)]
        public string Notes { get; set; }
        [MaxLength(500000)]
        public string Privileges { get; set; }
        public bool Locked { get; set; }
        public bool Hidden { get; set; }
        public ICollection<User> Users { get; set; }
    }

    public class Rank : LookUp { }

    public class Station : LookUp { } 

    public class ItemUnit : LookUp { } 

    public class Unit: LookUp { }
    
    public class MilitaryAlphabet:HasId
    {
        [Index(IsUnique = true), Required, MaxLength(1)]
        public string Alphabet { get; set; }
        [Index(IsUnique = true), Required, MaxLength(10)]
        public string Code { get; set; }
    }
}