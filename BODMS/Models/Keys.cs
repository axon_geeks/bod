﻿namespace BODMS.Models
{
    public class ConfigKeys
    {
        public static string Administrator = "Administrator";
        public static string Employee = "Employee";
    }
    public class Privileges
    {
        public const string CanViewDashboard = "Can View Dashboard";
        public const string CanViewAmendments = "Can View Amendments";
        public const string CanRequestAmendments = "Can Request Amendments";
        public const string CanApproveAmendments = "Can Approve Amendments";
        public const string CanDisapproveAmendments = "Can Disapprove Amendments";
        public const string CanActivateEmployees = "Can Activate Employees";
        public const string CanViewEmployees = "Can View Employees";
        public const string CanViewSettings = "Can View Settings";
        public const string CanEditSettings = "Can Edit Settings";
        public const string CanViewEmployeeData = "Can View Employee Data";
        public const string CanViewPersonalInfo = "Can View Personal Info";
        public const string CanManageUsers = "Can Manage Users";
    }

    public class GenericProperties
    {
        public const string CreatedBy = "CreatedBy";
        public const string CreatedAt = "CreatedAt";
        public const string ModifiedAt = "ModifiedAt";
        public const string ModifiedBy = "ModifiedBy";
        public const string Locked = "Locked";
    }

    public class ExceptionMessage
    {
        public const string RecordLocked = "Record is locked and can't be deleted.";
        public const string NotFound = "Record not found.";
    }

    public class SettingKeys
    {
        public static string Deposit = "Deposit";
        public static string Withdrawal = "Withdrawal";
    }

    public class DefaultKeys
    {
        public static string CurrencyFormat = "GHC##,###.00";
    }



    public class StoreInfo
    {
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
    }
}