﻿using System;
using System.Configuration;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using Microsoft.AspNet.Identity.EntityFramework;

namespace BODMS.Models
{
    public class AppDbContext : IdentityDbContext<User>
    {
        //Select Connection from Web.Config file
        public AppDbContext() : base("BODConnection") { }
        public DbSet<VaosSection> VaosSections { get; set; } 
        public DbSet<Store> Stores { get; set; } 
        public DbSet<Rank> Ranks { get; set; } 
        public DbSet<D982> D982 { get; set; } 
        public DbSet<D982Approval> D982Approvals { get; set; } 
        public DbSet<DailyReceiptSheet> DailyReceiptSheets { get; set; } 
        public DbSet<DailyReceiptSheetDetail> DailyReceiptSheetDetails { get; set; } 
        public DbSet<ReceiptVoucher> ReceiptVouchers { get; set; } 
        public DbSet<ReceiptVoucherDetail> ReceiptVoucherDetails { get; set; } 
        public DbSet<AuthorityDetail> AuthorityDetails { get; set; } 
        public DbSet<Authority> Authorities { get; set; } 
        public DbSet<AuthSource> AuthSources { get; set; } 
        public DbSet<Loan> Loans { get; set; } 
        public DbSet<LoadDetail> LoadDetails { get; set; } 
        public DbSet<AuthType> AuthTypes { get; set; } 
        public DbSet<AuthUnit> AuthUnits { get; set; } 
        public DbSet<Profile> Profiles { get; set; } 
        public DbSet<MilitaryAlphabet> MilitaryAlphabets { get; set; }

        public override int SaveChanges()
        {
            foreach (var entry in ChangeTracker.Entries()
                .Where(x => x.State == EntityState.Added)
                .Select(x => x.Entity)
                .OfType<IAuditable>())
            {
                entry.CreatedAt = DateTime.Now;
                entry.ModifiedAt = DateTime.Now;
            }

            foreach (var entry in ChangeTracker.Entries()
                .Where(x => x.State == EntityState.Modified)
                .Select(x => x.Entity)
                .OfType<IAuditable>())
            {
                entry.ModifiedAt = DateTime.Now;
            }
            return base.SaveChanges();
        }
    }
}