﻿using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.Linq;
using System.Web;

namespace BODMS.Models
{
    public class Authority : Auditable
    {
        public string Code { get; set; }
        public DateTime Date { get; set; }

        public virtual AuthSource AuthSource { get; set; }
        public long AuthSourceId { get; set; }

        public virtual AuthUnit AuthUnit { get; set; }
        public long AuthUnitId { get; set; }

        public string DrafterName { get; set; }
        public string DrafterApp { get; set; }
        public string SourcePhone { get; set; }

        public virtual AuthType AuthType { get; set; }
        public long AuthTypeId { get; set; }

        public bool IsRedStar { get; set; }
        public string Instructions { get; set; }
    }

    public class AuthorityDetail: HasId
    {
        public virtual MilitaryAlphabet Alphabet { get; set; }
        public long MilitaryAlphabetId { get; set; }

        public virtual Item Item { get; set; }
        public long ItemId { get; set; }

        public int Quantity { get; set; }

        public virtual Authority Authority { get; set; }
        public long AuthorityId { get; set; }
    }

    public class AuthSource : LookUp { }

    public class AuthType : LookUp { }

    public interface IUnit : IAuditable
    {
        string Code { get; set; }  
    }

    public class AuthUnit : LookUp, IUnit
    {
        public string Code { get; set; }
    }
}