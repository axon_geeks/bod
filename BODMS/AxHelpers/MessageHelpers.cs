﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using RestSharp;
using RestSharp.Authenticators;
using BODMS.DataAccess.Filters;
using BODMS.DataAccess.Repositories;
using BODMS.Models;

namespace SelfServiceApi.AxHelpers
{
    public class MessageHelpers
    {
        public static string GenerateRandomString(int length)
        {
            var stringBuilder = new StringBuilder(length);
            var chArray = "abcdefghijklmnopqrstuvwxyz0123456789_-".ToCharArray();
            var random = new Random((int)DateTime.Now.Ticks);
            for (var index = 0; index < length; ++index)
                stringBuilder.Append(chArray[random.Next(chArray.Length)]);
            return stringBuilder.ToString().ToLower();
        }
    }
}