using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using BODMS.Models;

namespace BODMS.DataAccess.Repositories
{
    public class ProfileRepository : BaseRepository<Item>
    {
        public override void Insert(Item item)
        {
            Dbset.Add(item);
            context.SaveChanges();
        }
    }
}