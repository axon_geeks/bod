﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BODMS.DataAccess.Filters;

namespace BODMS.DataAccess.Repositories
{
    public interface IBaseRepository<T> where T :class
    {
        long Count();
        long Count(Filter<T> filter);
        void Delete(long id);
        T Find(Filter<T> filter);
        List<T> Get();
        T Get(long id);
        List<T> Get(Filter<T> filter);
        void Insert(T entity);
        IQueryable<T> Query(Filter<T> filter);
        void SaveChanges();
        void Update(T entity);
    }
}