﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using BODMS.DataAccess.Filters;
using BODMS.Models;

namespace BODMS.DataAccess.Repositories
{
    public class BaseRepository <T> : IBaseRepository<T> where T : class
    {
        // A reference to the DBContext
        protected readonly AppDbContext context;

        //DbSet is a reference to the Table or Model via the use of Generics
        public DbSet<T> Dbset { get; set; }

        //Initializing the DBContext and the DbSet via the constructor
        public BaseRepository()
        {
            context = new AppDbContext();
            Dbset = context.Set<T>();
        }
        
        public T Find(Filter<T> filter) { return filter.BuildQuery(Dbset.Select(x => x)).FirstOrDefault(); }

        public T Get(long id) { return Dbset.Find(id); }

        public List<T> Get() { return Dbset.ToList(); }

        public List<T> Get(Filter<T> filter) { return filter.BuildQuery(Dbset.Select(x => x)).ToList(); }

        public long Count() { return Dbset.Count(); }

        public long Count(Filter<T> filter) { return filter.BuildQuery(Dbset.Select(x => x)).Count(); }

        public virtual IQueryable<T> Query(Filter<T> filter) { return filter.BuildQuery(Dbset.Select(x => x)); }

        public virtual void Insert (T entity)
        {
            Dbset.Add(entity);
            context.SaveChanges();
        }

        public virtual void SaveChanges() { context.SaveChanges(); }

        public virtual void Update(T entity) {

            context.Entry(entity).State = EntityState.Modified;
            context.SaveChanges();
        }

        public virtual void Delete(long id)
        {
            var record = Dbset.Find(id);
            var hasLock = typeof(T).GetProperty(GenericProperties.Locked);
            if(hasLock != null)
            {
                var isLocked = (bool)hasLock.GetValue(record, null);
                if (isLocked) throw new Exception(ExceptionMessage.RecordLocked);
            }

            Dbset.Remove(record);
        }
    }
}